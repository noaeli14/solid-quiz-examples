package Question2;

public interface Cipher {
    String encrypt(String textToEncrypt);
    String decrypt(String textToDecrypt);
}
